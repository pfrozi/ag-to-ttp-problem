#include "../include/AgAlgol.h"

AgAlgol::AgAlgol(){

    cRate = 0;
    pRate = 0;
    divRate = 0;

    generation = 1;
    genNoImprov = 0;

    current = new Population;
    startClock = clock();
}


AgAlgol::~AgAlgol(){

}

void AgAlgol::SetLengthPop(int length){

    lengthPop = length;
}

void AgAlgol::generateInitial(int length){

    SetLengthPop(length);
    generateInitial();
}

void AgAlgol::generateInitial(){

    current->SetInstanceLen(instance_len);
    current->SetChromoLen(chromo_len);
    current->SetMatrix(matrix);
    current->SetLength(lengthPop);
    current->GenerateRandom(lengthPop, chromo_len);
}

void AgAlgol::Solve(int type){

    solveType = type;

    generateInitial();
    current->CalcFitness(solveType, roulette_on);

    do{

        //current->PrintIndividuals();

        // Realiza a selecao dos pais
        current->SelectParents(eliteRate, divRate, solveType);

        // Realiza a reproducao por mutacao E crossover
        current->ParentsMutation(pRate, mRate);
        current->ParentsCrossover(cRate);

        nextGeneration();

        endClock = clock();

    }while(!verifyStoppage()); // verifica condicao de parada


    printf("\n");
    printf("%s", printTTPInstance(names, current->GetBestIndividual()->GetChromo(),instance_len).c_str());
    printf("\n");
    printf("\nDistancia Total: %.3f", current->GetBestIndividual()->fitness);
    printf("\nQuant. Geracoes: %d", (int)generation);
    printf("\nTempo total: %.5f segundos", ((float)endClock-startClock)/CLOCKS_PER_SEC);
    printf("\n\n");



    export_report("report.log"
                 , (char*)inst_name.c_str()
                 , lengthPop
                 , instance_len
                 , cRate
                 , pRate
                 , eliteRate
                 , mRate
                 , divRate
                 , stopGen
                 , stopTime
                 , current->GetBestIndividual()->fitness
                 , (int)generation
                 , ((float)endClock-startClock)/CLOCKS_PER_SEC);

    delete current;
}

void AgAlgol::nextGeneration(){

    Population* nextPop = new Population;

    current->GenerateNewPopulation(nextPop);

    generation++;

    nextPop->CalcFitness(solveType, roulette_on);

    bool noImprove = false;

    if(solveType==SOLVE_MIN){

        noImprove = nextPop->GetBestFitness()>=current->GetBestFitness();
    }
    else{

        noImprove = nextPop->GetBestFitness()<=current->GetBestFitness();
    }

    if(noImprove) {
        genNoImprov++;
    }
    else{
        genNoImprov = 0;
    }

    delete current;
    current = nextPop;

}

double AgAlgol::TimeElapsedInMinutes(){

    return (((float)endClock-startClock)/CLOCKS_PER_SEC)/60.0;
}

std::string AgAlgol::StrTimeElapsed(){

    std::stringstream out;

    out << ((float)endClock-startClock)/CLOCKS_PER_SEC;
    out << " segundos";

    return out.str();
}


void AgAlgol::SetPRate(float rate){

    pRate = rate;
    // eliteRate  = 1.0f - (pRate+cRate);
}

void AgAlgol::SetCRate(float rate){

    cRate = rate;
    // eliteRate  = 1.0f - (pRate+cRate);
}

void AgAlgol::SetMRate(float rate){

    mRate = rate;
}

void AgAlgol::SetEliteRate(float rate){

    eliteRate = rate;
}

void AgAlgol::SetStopInGenerations(int n){

    stopGen = n;
}

void AgAlgol::SetStopInTime(int minutes){

    stopTime = minutes;
}

bool AgAlgol::verifyStoppage(){

    // Parada por tempo OU parada por quantidade de geracoes sem melhora
    bool time = (TimeElapsedInMinutes()>stopTime);
    bool gen = (genNoImprov>stopGen);

    return(time || gen);
}



void AgAlgol::SetNamesFromFile(std::string file){

    names = readStringVector(file);
}
void AgAlgol::SetMatrixFromFile(std::string file){

    matrix = readFloatMatrix(file);
}
void AgAlgol::SetFileOut(std::string file){

    file_out = file;
}
void AgAlgol::SetInstanceLen(int length){

    instance_len = length;
}

std::string AgAlgol::PrintDistMatrix(){

    return printFloatMatrix(matrix, instance_len);
}
std::string AgAlgol::PrintNames(){

    return printStringVector(names);
}

void AgAlgol::SetInstanceSize(int s){

    instance_len = s;
    chromo_len   = s*s-s;
}
void AgAlgol::SetDivRate(float rate_div){

    divRate = rate_div;
}
void AgAlgol::SetRoulette(bool roulette){

    roulette_on = roulette;
}
void AgAlgol::SetInstanceName(std::string name){

    inst_name = name;
}
