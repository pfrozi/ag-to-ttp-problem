#include "../include/AgAlgol.h"
#include "../include/Util.h"


#define RETURN_SUCESS      0
#define RETURN_NOTHING     0
#define RETURN_ERROR       1
#define ERROR_INVALID_ARGS 2

#define ARGS_MAX           22

// args to AGSolver ;)
#define ARGS_HELP       "-h"  // Option para imprimir a lista de opcoes

#define ARGS_RATE_P     "-P"
#define ARGS_RATE_C     "-C"
#define ARGS_RATE_M     "-M"
#define ARGS_RATE_ELITE "-E"

#define ARGS_RATE_DIV   "-D"
#define ARGS_RATE_ROUL  "-R"

#define ARGS_POP_LEN    "-L"

#define ARGS_STOP_GEN   "-g"
#define ARGS_STOP_TIME  "-t"

#define ARGS_INST_NAME  "-a"

// args to TTP problem
#define ARGS_SIZE_N      "-N"

#define ARGS_FILE_MATRIX "-i"
#define ARGS_FILE_NAMES  "-n"
#define ARGS_FILE_OUT    "-o"

float rate_p, rate_c, rate_m, rate_elite, rate_div;
bool roulette;
int stop_gen, stop_time;
int length_pop, size_n;

std::string file_matrix, file_out, file_names, instance_name;

AgAlgol* ga;

void help(){

    printf("  _____   ___   _____       _                  _____ ___________       \n");
    printf(" |  __ \\ / _ \\ /  ___|     | |                |_   _|_   _| ___ \\   \n");
    printf(" | |  \\// /_\\ \\\\ `--.  ___ | |_   _____ _ __    | |   | | | |_/ /  \n");
    printf(" | | __ |  _  | `--. \\/ _ \\| \\ \\ / / _ \\ '__|   | |   | | |  __/  \n");
    printf(" | |_\\ \\| | | |/\\__/ / (_) | |\\ V /  __/ |      | |   | | | |      \n");
    printf("  \\____/\\_| |_/\\____/ \\___/|_| \\_/ \\___|_|      \\_/   \\_/ \\_| \n");
    printf("                                                                       \n");

    printf("Parâmetros do GASolver: \n");
    printf("\t-L: Tamanho da população Inicial; \n");
    printf("\t-C: Taxa de indivíduos provenientes CrossOver; \n");
    printf("\t-P: Taxa de indivíduos provenientes de Mutação; \n");
    printf("\t-M: Taxa de mutação dentro do cromossomo; \n");
    printf("\t-E: Taxa de indivíduos provenientes Elitismo; \n");
    printf("\t-D: Taxa de diversidade dentro do grupo elitista; \n");
    printf("\t-R: Ativa a roleta; \n");

    printf("\t\n");
    printf("\t-g: Quantidade máxima de gerações sem melhora (critério de parada); \n");
    printf("\t-t: Tempo máximo de execução (critério de parada); \n");
    printf("\t\n");
    printf("Parâmetros específicos do TTP: \n");
    printf("\t-i: Arquivo de entrada com a matriz de distâncias entre as cidades; \n");
    printf("\t-n: Arquivo de entrada com os nomes dos locais; \n");
    printf("\t-o: Caminho do arquivo de saída com a solução do problema; \n");
    printf("\t-N: Quantidade de Cidades; \n");
    printf("\t\n");

}

void setGa(){

    ga->SetPRate(rate_p);
    ga->SetCRate(rate_c);
    ga->SetMRate(rate_m);
    ga->SetEliteRate(rate_elite);
    ga->SetLengthPop(length_pop);
    ga->SetStopInGenerations(stop_gen);
    ga->SetStopInTime(stop_time);

    ga->SetNamesFromFile(file_names);
    ga->SetMatrixFromFile(file_matrix);
    ga->SetFileOut(file_out);

    ga->SetInstanceSize(size_n);
    ga->SetInstanceName(instance_name);

    ga->SetDivRate(rate_div);
    ga->SetRoulette(roulette);

    //srand(MY_SEED);
    std::srand(std::time(0));
}

void print_ga_args(){

    printf(" - Parametros do AG - \n");
    printf("\tTaxa de ind. mutados: %.4f\n", rate_p);
    printf("\tTaxa de mutacao no chromo: %.4f\n", rate_m);
    printf("\tTaxa de ind. crossover: %.4f\n", rate_c);
    printf("\tTaxa de elitismo: %.4f\n", rate_elite);
    printf("\tTamanho da população inicial: %d\n", length_pop);
    printf(" - Criterios de Parada - \n");
    printf("\tQuant. de geracoes sem melhora: %d\n", stop_gen);
    printf("\tTempo (em minutos): %d\n", stop_time);

    printf("\n- Matrix de Distancias -\n");
    printf("Tamanho da instancia: %d\n\n", size_n);
    std::string s = ga->PrintDistMatrix();

    printf("%s", s.c_str());

    printf("\n- Lista de Nomes -\n");

    std::string s2 = ga->PrintNames();

    printf("%s", s2.c_str());
}

int main(int argc, char* argv[]){

    roulette = false;

    ga = new AgAlgol;

    if (argc < ARGS_MAX || (argc==1 && !(strcmp(argv[1],ARGS_HELP)==0) )) {

        help();
        return (ERROR_INVALID_ARGS);

    } else {

        for (int i = 1; i < argc; i+=2) {

            if (i + 1 != argc)
            {
                std::string arg = argv[i+1];

                if (strcmp(argv[i],ARGS_HELP) == 0) {

                    help();
                    return (RETURN_NOTHING);


                } else if (strcmp(argv[i], ARGS_HELP)==0) {

                    help();

                } else if (strcmp(argv[i], ARGS_RATE_P)==0) {

                    rate_p = atof(arg.c_str());

                } else if (strcmp(argv[i], ARGS_RATE_C)==0) {

                    rate_c = atof(arg.c_str());

                } else if (strcmp(argv[i], ARGS_RATE_M)==0) {

                    rate_m = atof(arg.c_str());

                } else if (strcmp(argv[i], ARGS_RATE_ELITE)==0) {

                    rate_elite = atof(arg.c_str());

                } else if (strcmp(argv[i], ARGS_STOP_GEN)==0) {

                    stop_gen = atoi(arg.c_str());

                } else if (strcmp(argv[i], ARGS_STOP_TIME)==0) {

                    stop_time = atoi(arg.c_str());

                } else if (strcmp(argv[i], ARGS_POP_LEN)==0) {

                    length_pop = atoi(arg.c_str());

                } else if (strcmp(argv[i], ARGS_FILE_MATRIX)==0) {

                    file_matrix = arg;

                } else if (strcmp(argv[i], ARGS_FILE_OUT)==0) {

                    file_out = arg;

                } else if (strcmp(argv[i], ARGS_FILE_NAMES)==0) {

                    file_names = arg;

                } else if (strcmp(argv[i], ARGS_SIZE_N)==0) {

                    size_n = atoi(arg.c_str());

                } else if (strcmp(argv[i], ARGS_RATE_DIV)==0) {

                    rate_div = atof(arg.c_str());

                } else if (strcmp(argv[i], ARGS_RATE_ROUL)==0) {

                    roulette = true;

                } else if (strcmp(argv[i], ARGS_INST_NAME)==0) {

                    instance_name = arg;

                } else {

                    return (ERROR_INVALID_ARGS);

                }
            }
        }

    }

    setGa();

    print_ga_args();

    ga->Solve(SOLVE_MIN);

    return RETURN_SUCESS;
}

