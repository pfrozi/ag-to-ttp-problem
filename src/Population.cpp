
#include "../include/Population.h"

Population::Population()
{
    //ctor
    bestParents = new std::vector<IIndividual*>;
    childrenCrossover = new std::vector<IIndividual*>;
    childrenMutation = new std::vector<IIndividual*>;


    individuals     = NULL;
    bestIndividual  = NULL;

    sumRoulette = 0;
    roulette_on = false;

}

Population::~Population()
{

    bestParents->clear();

    for(unsigned int i=0;i<childrenCrossover->size();i++) delete childrenCrossover->at(i);
    childrenCrossover->clear();

    for(unsigned int i=0;i<childrenMutation->size();i++) delete childrenMutation->at(i);
    childrenMutation->clear();

    for(int i=0;i<length;i++) delete individuals[i];

    delete individuals;

    delete bestParents;
    delete childrenCrossover;
    delete childrenMutation;


}

void Population::SetLength(int popLength){

    length = popLength;

    if(individuals!=NULL){
        for(int i=0;i<length;i++) delete individuals[i];
        delete individuals;
    }
    if(bestIndividual!=NULL) delete bestIndividual;
    individuals = new IIndividual*[popLength];

    // isso causará leak?
    for(int i=0;i<length;i++){

        individuals[i] = IIndividual::Create(chromo_len,instance_len);
        individuals[i]->SetDistMatrix(matrix);
    }

}

void Population::GenerateRandom(int popLength, int chromoLength){

    SetChromoLen(chromoLength);

    for(int l=0;l<length;l++){

        individuals[l]->GenerateRdmAlleles();
    }

}

void Population::CalcFitness(int type, bool roulette){

    float curr = 0, sum = 0;
    bool currBest=0, currWorst=0;

    roulette_on = roulette;

    if(type==SOLVE_MIN){

        bestFitness = std::numeric_limits<float>::max();
        worstFitness = std::numeric_limits<float>::min();
    }else {

        bestFitness = std::numeric_limits<float>::min();
        worstFitness = std::numeric_limits<float>::max();
    }

    for(int i=0;i<length;i++){

        if(!individuals[i]->has_fitness){

            curr = individuals[i]->CheckFitness();
        }else{

            curr = individuals[i]->fitness;
        }

        if(type==SOLVE_MIN){

            currBest  = curr<bestFitness;
            currWorst = curr>worstFitness;
        }else {

            currBest  = curr>bestFitness;
            currWorst = curr<worstFitness;
        }

        if(currBest){

            bestFitness = curr;
            setBestIndividual(individuals[i]);

        }
        else if(currWorst){

            worstFitness = curr;

        }
        sum += curr;

        sumRoulette = 1.0f / curr;

    }
    avgFitness = sum / (float)length;

}

void Population::setBestIndividual(IIndividual* i){

    bestIndividual = i;

}


bool const compareIndividualsMin(const IIndividual* lhs,const IIndividual* rhs)
{

  return lhs->fitness < rhs->fitness;
}
bool const compareIndividualsMax(const IIndividual* lhs,const IIndividual* rhs)
{

  return lhs->fitness > rhs->fitness;
}

void Population::SelectParents(float eliteRate, float rate_div, int type){

    int parentsLen    = floor(length*eliteRate);
    int parentsDivLen = floor(parentsLen*rate_div);

    for(int i=0;i<length;i++){

        bestParents->push_back(individuals[i]);

    }

    if(type==SOLVE_MIN){

        std::sort(bestParents->begin(),bestParents->end(),compareIndividualsMin);
    }
    else {

        std::sort(bestParents->begin(),bestParents->end(),compareIndividualsMax);
    }

    bestParents->resize(parentsLen-parentsDivLen);

    if(rate_div>0){

        for(int i=0;i<parentsDivLen;i++){

            int parent = GetRdmInt(0,length-1);
            bestParents->push_back(individuals[parent]);

        }
    }

}

void Population::ParentsMutation(float pRate, float mRate){

    int mutateLen = floor(length*pRate);
    int parent1;

    // if(!roulette_on)


    for(int i=0;i<mutateLen;i++){
        parent1 = GetRdmInt(0,bestParents->size()-1);

       //parent1 = GetRdmInt(0, length);

        IIndividual* newIndividual = IIndividual::Create(chromo_len, instance_len);
        newIndividual->SetDistMatrix(matrix);

        bestParents->at(parent1)->Mutate(mRate,newIndividual);
        //individuals[parent1]->Mutate(mRate,newIndividual);

        childrenMutation->push_back(newIndividual);
    }


}

void Population::ParentsCrossover(float cRate){

    int crossLen = floor(length*cRate);
    unsigned int parent1,parent2;

    for(int i=0;i<crossLen;i++){
        parent1 = GetRdmInt(0,bestParents->size()-1);

        if(parent1>(bestParents->size()/2)){
            parent2 = GetRdmInt(0,parent1-1);
        }
        else{
            parent2 = GetRdmInt(parent1+1,bestParents->size()-1);
        }

        IIndividual* newIndividual = IIndividual::Create(chromo_len, instance_len);
        newIndividual->SetDistMatrix(matrix);

        // crossover entre o individuo de indice parent1 e o individuo de indice parent2
        bestParents->at(parent1)->Crossover(
                                 bestParents->at(parent2)
                                ,newIndividual);

        childrenCrossover->push_back(newIndividual);

    }

}


void Population::GenerateNewPopulation(Population* newPopulation){

    int newLength = bestParents->size()+childrenCrossover->size()+childrenMutation->size();

    newPopulation->SetChromoLen(chromo_len);
    newPopulation->SetInstanceLen(instance_len);
    newPopulation->SetLength(newLength);
    newPopulation->SetMatrix(matrix);

    int k = 0;
    for(unsigned int l=0;l<bestParents->size();l++){
        newPopulation->CopyIndividual(k,bestParents->at(l), true);
        k++;
    }
    for(unsigned int i=0;i<childrenCrossover->size();i++){
        newPopulation->CopyIndividual(k,childrenCrossover->at(i), false);
        k++;
    }
    for(unsigned int j=0;j<childrenMutation->size();j++){
        newPopulation->CopyIndividual(k,childrenMutation->at(j), false);
        k++;
    }

}

float Population::GetBestFitness(){

    return bestFitness;
}

IIndividual* Population::GetBestIndividual(){

    return bestIndividual;
}

void Population::CopyIndividual(int index, IIndividual* individual, bool copy_fitness){

    individual->CopyTo(individuals[index], copy_fitness);
}
void Population::PrintIndividuals(){

    for(int i=0;i<length;i++){

        printf("\nIndividual %d: ",i);
        for(int j=0;j<chromo_len;j++){

            printf("%d",individuals[i]->GetAllele(j));
        }
        printf("\nFitness %d: %f",i, individuals[i]->fitness);
    }
}

void Population::SetChromoLen(int length){

    chromo_len = length;
}
void Population::SetInstanceLen(int length){

    instance_len = length;
}

void Population::SetMatrix(float** in_matrix){

    matrix = in_matrix;
}
