#include "../include/IndividualTTP.h"

IIndividual* IIndividual::Create(int chromo_len, int instance_len){

    return (new IndividualTTP(chromo_len, instance_len));
}


IndividualTTP::IndividualTTP(int chromo_len, int instance_len){

    length = chromo_len;

    teams = instance_len;

    len_games = (length / teams)*2;

    if(chromosome!=NULL){

        //delete chromosome;
        chromosome = NULL;
    }

    chromosome = new int[length];

    memset(chromosome,-1,length*sizeof(length));

    has_fitness = false;
}

IIndividual::~IIndividual(){

    delete chromosome;
}

int  IndividualTTP::GetAllele(int index){

    return chromosome[index];
}
void IndividualTTP::SetAllele(int index, int value){

    chromosome[index] = value;
}

void IndividualTTP::Crossover(IIndividual* individual, IIndividual* newIndividual){

    // select games/2 game to swap
    individual->CopyTo(newIndividual, false);

    for(int cross=0; cross<(length / 4); cross++){

        int swapped = rand()%length;

        if(chromosome[swapped]!=newIndividual->GetAllele(swapped)){

            int k = swapped;

            for(int j=0;j<length;j++){

                k++;
                k%=length;

                if(newIndividual->GetAllele(k)==chromosome[swapped]){
                    newIndividual->SwapAllele(k, swapped);
                    break;
                }

            }
        }


    }
}

void IndividualTTP::CopyTo(IIndividual* newIndividual, bool copy_fitness){

    newIndividual->SetDistMatrix(dist_matrix);

    if(copy_fitness && has_fitness){

        newIndividual->has_fitness = true;
        newIndividual->fitness     = fitness;
    }else{

        newIndividual->has_fitness = false;
    }

    for(int i=0;i<length;i++){

        newIndividual->SetAllele(i, chromosome[i]);
    }
}

void IndividualTTP::Mutate(float mRate, IIndividual* newIndividual){

    CopyTo(newIndividual, false);

    for(int i=0;i<length;i++){

        if(GetRdmBool(mRate)){
            int swapped = rand()%length;
            if(i==swapped){
                swapped++;
                swapped %= length;
            }
            newIndividual->SwapAllele(i, swapped);
        }
    }
}

void IndividualTTP::SwapAllele(int a, int b){

    int aux       = chromosome[a];
    chromosome[a] = chromosome[b];
    chromosome[b] = aux;

}

void IndividualTTP::GenerateRdmAlleles(){

    int games[len_games];
    memset(games,0,(len_games)*sizeof(int));

    for(int i=0;i<length;i++){

        int sel_game = rand()%(len_games);

        if(games[sel_game]>=(teams/2)){

            for(int j=0;j<len_games;j++){

                if(games[j]<(teams/2)){
                    sel_game = j;
                    break;
                }
            }
        }

        games[sel_game]++;
        chromosome[i] = sel_game;
    }
}

int* IndividualTTP::GetChromo(){

    return chromosome;
}

float IndividualTTP::CheckFitness(){

    float sum  = 0;

    int prev_game[teams];
    int games_home[teams];
    int games_out[teams];
    bool same_game[teams];

    for(int k=0;k<teams;k++) prev_game[k]=k;

    memset(games_home, 0, teams*sizeof(teams));
    memset(games_out, 0, teams*sizeof(teams));

    for(int game=0;game<(len_games);game++){

        int games_v = 0;
        memset(same_game, false, teams*sizeof(bool));

        for(int i=0;i<length && games_v<teams/2;i++){

            if(chromosome[i]==game){

                games_v++;

                int teamA = getA(i);
                int teamB = getB(i);

                games_home[teamA]++;
                games_home[teamB] = 0;
                games_out[teamB]++;
                games_out[teamA] = 0;

                // verify where where the teams A and B on previous game
                int prev_teamA = prev_game[teamA];
                int prev_teamB = prev_game[teamB];

                sum += dist_matrix[prev_teamA][teamA];
                sum += dist_matrix[prev_teamB][teamA];

                // penalty PENALTY_3HOME
                if(games_home[teamA]>3){
                    sum += PENALTY_3HOME;
                }

                // penalty PENALTY_3OUT
                if(games_out[teamB]>3){
                    sum += PENALTY_3OUT;
                }

                // penalty PENALTY_SAME_GAME
                if(same_game[teamA]||same_game[teamB]){
                    sum += PENALTY_SAME_GAME;
                }

                // penalty PENALTY_CONSEC
                if(prev_game[teamA] == teamB){
                    sum += PENALTY_SAME_GAME;
                }


                prev_game[teamA] = teamA;
                prev_game[teamB] = teamA;

                same_game[teamA] = true;
                same_game[teamB] = true;

            }
        }
    }

    for(int l=0;l<teams;l++){

        if(games_out[l]>0){
            sum += dist_matrix[l][prev_game[l]];
        }
    }

    has_fitness = true;
    fitness = sum;

    return fitness;
}



int IndividualTTP::getA(int i){

    return (i/(teams-1));
}
int IndividualTTP::getB(int i){

    int teamB = i % (teams-1);

    if(getA(i)<=teamB){
        teamB++;
    }
    return teamB;

}

void IndividualTTP::SetDistMatrix(float** matrix){

    dist_matrix = matrix;
}
