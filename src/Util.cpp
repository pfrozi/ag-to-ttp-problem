#include "../include/Util.h"

std::vector<std::string>* split(std::string str,std::string sep){

    char* cstr    = const_cast<char*>(str.c_str());
    char* current = NULL;
    std::vector<std::string>* arr = new std::vector<std::string>;
    current=strtok(cstr,sep.c_str());
    while(current!=NULL){
        arr->push_back(current);
        current=strtok(NULL,sep.c_str());
    }
    return arr;
}


float** readFloatMatrix(std::string strFile){

    float** matrix;
    bool    first_line = true;
    int     i = 0;

    std::string               line;
    std::ifstream             file;
    std::vector<std::string>* p_split = NULL;

    file.open(strFile.c_str(), std::fstream::out);

    while (std::getline(file, line))
    {
        p_split = split(line," ");

        if(p_split!=NULL && p_split->size()>0){

            if(first_line){
                matrix = new float*[p_split->size()];
                first_line = false;
            }
            matrix[i] = new float[p_split->size()];

            for(unsigned int j=0;j<p_split->size();j++){

                matrix[i][j] = atof(p_split->at(j).c_str());
            }

            i++;
        }

        delete p_split;
    }

    file.close();

    return matrix;
}

std::vector<std::string>* readStringVector(std::string strFile){

    int buff_size = 10*1024;
    char buffer[buff_size];

    std::ifstream             file;
    std::vector<std::string>* p_split = NULL;

    file.open(strFile.c_str(), std::fstream::out);

    file.read(buffer, buff_size);

    std::string text(buffer);

    p_split = split(text,"\n");


    file.close();

    return p_split;
}



bool GetRdmBool(float p)
{
    return (std::rand() / (double)RAND_MAX) < p;
}

int  GetRdmInt(int begin, int end){

    return(begin + (std::rand() % (end - begin)));

}

bool ExistItem(int size,int* v, int value){

    for(int i=0;i<size;i++){
        if(v[i]==value) return true;
    }

    return false;

}

std::string printStringVector(std::vector<std::string>* l){

    std::string r = "";

    for(unsigned int i=0;i<l->size();i++){

        r.append(l->at(i));
        r.append("\n");
    }
    return r;
}

std::string printFloatMatrix(float** matrix, unsigned int m_size){

    std::string r = "";

    for(unsigned int i=0;i<m_size;i++){

        for(unsigned int j=0;j<m_size;j++){

            char s[100];

            snprintf( s
                    , 100
                    , "%-06.f"
                    , matrix[i][j]);

            r.append(s);
            r.append(" ");
        }

        r.append("\n");
    }

    return r;
}

void ConvertChromo(int* chromosome, int length, float* x, float* y){

    *x=0;
    *y=0;
    int sign_x=0, sign_y=length/2;

    for(int i=1;i<length/2;i++){

        *x += pow(2,(length/2-1)-i) * chromosome[i];
    }

    if(chromosome[sign_x]==1){
        *x = *x*-1.0f;
    }

    for(int j=length/2+1;j<length;j++){

        *y += pow(2,(length-1)-j) * chromosome[j];
    }

    if(chromosome[sign_y]==1){
        *y = *y*-1.0f;
    }

    *x = *x/1000.0f;
    *y = *y/1000.0f;

}

std::string printTTPInstance(std::vector<std::string>* names, int* chromo, int inst_size){

    std::string r = "";

    int length = inst_size*inst_size-inst_size;
    int len_games = (length / inst_size)*2;

    r.append("Chromo: ");
    for(int i = 0; i<length;i++) {
        r.append(std::to_string(chromo[i]));
        r.append(" ");
    }
    r.append("\n\n");

    for(int j = 0; j<inst_size;j++){

        r.append(names->at(j));
        r.append("   ");

    }
    r.append("\n");
    for(int j = 0; j<inst_size;j++){

        r.append("---");
        r.append("   ");

    }
    r.append("\n");
    for(int game = 0; game<len_games;game++){

        for(int team = 0; team<inst_size;team++){

            for(int i = 0; i<length;i++){

                if(chromo[i]==game){

                    int teamA = (i/(inst_size-1));
                    int teamB = i % (inst_size-1);

                    if(teamA<=teamB){
                        teamB++;
                    }

                    if(team==teamA){
                        r.append(names->at(teamB));
                        r.append("   ");
                        break;
                    }else if(team==teamB){
                        r.append("@");
                        r.append(names->at(teamA));
                        r.append("  ");
                        break;
                    }
                }

            }
        }
        r.append("\n");
    }



    return r;
}

int export_report(  char *report
                  , char *instance_name
                  , int pop_length
                  , int instance_len
                  , float c_rate
                  , float p_rate
                  , float e_rate
                  , float m_rate
                  , float d_date
                  , int stop_gen
                  , int stop_time
                  , float fitness
                  , int generations
                  , float time){

    FILE *pfile = fopen(report, FILE_MODE_APPEND);

    if(pfile == NULL){
        return EXPORT_ERROR;
    }
    fprintf(pfile, "%s \t ", instance_name);
    fprintf(pfile, "%d \t ", pop_length);
    fprintf(pfile, "%d \t ", instance_len);
    fprintf(pfile, "%2.3f \t ", c_rate);
    fprintf(pfile, "%2.3f \t ", p_rate);
    fprintf(pfile, "%2.3f \t ", e_rate);
    fprintf(pfile, "%2.3f \t ", m_rate);
    fprintf(pfile, "%2.3f \t ", d_date);

    fprintf(pfile, "%d \t ", stop_gen);
    fprintf(pfile, "%d \t ", stop_time);
    fprintf(pfile, "%.3f \t " , fitness);
    fprintf(pfile, "%d \t ", generations);
    fprintf(pfile, "%.5f \t ", time);

    fprintf(pfile, "\n");
    fclose(pfile);

    return EXPORT_OK;
}
