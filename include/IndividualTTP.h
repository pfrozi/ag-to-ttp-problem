
#ifndef IndividualTTP_H
#define IndividualTTP_H

#include "IIndividual.h"
#include <stdlib.h>
#include "Util.h"
#include <math.h>

#define PENALTY_3HOME     pow(10,12)
#define PENALTY_3OUT      pow(10,11)
#define PENALTY_SAME_GAME pow(10,10)
#define PENALTY_CONSEC    pow(10,9)



class IndividualTTP : public IIndividual {

    public:
        IndividualTTP(int chromo_len, int instance_len);

        void          Crossover(IIndividual* individual, IIndividual* newIndividual);
        void          Mutate(float mRate, IIndividual* newIndividual);

        void          GenerateRdmAlleles();

        int           GetAllele(int index);
        void          SetAllele(int index, int value);

        float         CheckFitness();

        int*          GetChromo();

        void          SetDistMatrix(float** matrix);

        void          SwapAllele(int a, int b);
        void          CopyTo(IIndividual* newIndividual, bool copy_fitness);

    private:


        int   getA(int i);
        int   getB(int i);

        int     len_games;
        int     teams;
        float** dist_matrix;

};

#endif // IndividualTTP_H
