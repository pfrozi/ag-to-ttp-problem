
#ifndef GATTP_H
#define GATTP_H

#include <iostream>
#include <sstream>
#include <list>
 
#include "Population.h"
#include "Util.h"

class AgAlgol
{
    public:


        AgAlgol();
        virtual ~AgAlgol();


        void Solve(int type);

        double TimeElapsedInMinutes();
        std::string StrTimeElapsed();

        void SetLengthPop(int length);
        void SetPRate(float rate);
        void SetCRate(float rate);
        void SetMRate(float rate);
        void SetEliteRate(float rate);
        void SetStopInGenerations(int n);
        void SetStopInTime(int minutes);
        void SetInstanceLen(int length);
        void SetDivRate(float rate_div);
        void SetRoulette(bool roulette);

        void SetNamesFromFile(std::string file);
        void SetMatrixFromFile(std::string file);
        void SetFileOut(std::string file);

        void SetInstanceName(std::string name);

        void SetInstanceSize(int s);

        std::string PrintDistMatrix();
        std::string PrintNames();

    protected:

    private:

        float**                   matrix;
        std::vector<std::string>* names;

        std::string               file_out;
        std::string               inst_name;

        int solveType;

        int   lengthPop;
        float pRate;
        float cRate;
        float mRate;
        float eliteRate;
        float divRate;

        int   stopGen;
        int   stopTime;
        int   instance_len;

        bool roulette_on;

        int   chromo_len;

        time_t startClock, endClock;

        Population* current;

        long        generation;
        int         genNoImprov;

        void nextGeneration();
        bool verifyStoppage();

        void generateInitial();
        void generateInitial(int length);



};

#endif // GATTP_H
