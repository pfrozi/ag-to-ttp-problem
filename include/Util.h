
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stdio.h>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <cstdlib>
#include <ctime>
#include <string.h>
#include <math.h>

#define EXPORT_ERROR    1
#define EXPORT_OK       0

#define FILE_MODE_READ "r"
#define FILE_MODE_WRITE "w"
#define FILE_MODE_APPEND "a+"

//#define MY_SEED 314159265

#define CHROMO_LEN 24

#define SOLVE_MIN   0
#define SOLVE_MAX   1

std::vector<std::string>* split(std::string str,std::string sep);
float** readMatrix(std::string strMatrix, std::vector<std::string> *header, float** matrix);
bool GetRdmBool(float p);
int  GetRdmInt(int begin, int end);

bool ExistItem(int size, int* v, int value);

void ConvertChromo(int* chromosome, int length, float* x, float* y);

float** readFloatMatrix(std::string strFile);
std::vector<std::string>* readStringVector(std::string strFile);

std::string printStringVector(std::vector<std::string>* l);
std::string printFloatMatrix(float** matrix, unsigned int m_size);


std::string printTTPInstance(std::vector<std::string>* names, int* chromo, int inst_size);

int export_report(  char *report
                  , char *instance_name
                  , int pop_length
                  , int instance_len
                  , float c_rate
                  , float p_rate
                  , float e_rate
                  , float m_rate
                  , float d_date
                  , int stop_gen
                  , int stop_time
                  , float fitness
                  , int generations
                  , float time);
