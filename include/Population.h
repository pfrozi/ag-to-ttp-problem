#ifndef POPULATION_H
#define POPULATION_H

#include <vector>
#include <limits>       // std::numeric_limits
#include <algorithm>
#include <math.h>

#include "IIndividual.h"
#include "Util.h"


#include "IndividualTTP.h"

class Population
{
    public:

        Population();
        virtual ~Population();

        void GenerateRandom(int popLength, int chromoLength);

        void SetLength(int popLength);
        void CalcFitness(int type, bool roulette);

        void SelectParents(float eliteRate, float rate_div, int type);
        void ParentsCrossover(float cRate);
        void ParentsMutation(float pRate, float mRate);

        void SetDistMatrix(float** matrix);

        void GenerateNewPopulation(Population* newPopulation);

        float        GetBestFitness();
        IIndividual* GetBestIndividual();

        void CopyIndividual(int index, IIndividual* individual, bool copy_fitness);
        void PrintIndividuals();

        void SetChromoLen(int length);

        void SetMatrix(float** in_matrix);

        void SetInstanceLen(int length);

    protected:

    private:

        int         length;
        int         nTeams;
        int         rounds;

        int         chromo_len;

        float       bestFitness;
        float       worstFitness;
        float       avgFitness;

        float       sumRoulette;
        bool        roulette_on;

        IIndividual*  bestIndividual;
        IIndividual** individuals;

        std::vector<IIndividual*>* bestParents;
        std::vector<IIndividual*>* childrenCrossover;
        std::vector<IIndividual*>* childrenMutation;

        void setBestIndividual(IIndividual* i);

        float**     matrix;
        int         instance_len;

};

#endif // POPULATION_H
